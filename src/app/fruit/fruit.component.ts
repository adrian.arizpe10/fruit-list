import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Fruit } from '../models/fruit';
import { FormBuilder, FormGroup } from '@angular/forms';
import { on } from 'process';
@Component({
  selector: 'app-fruit',
  templateUrl: './fruit.component.html',
  styleUrls: ['./fruit.component.sass'],
})
export class FruitComponent implements OnInit {
  result: Fruit[] = [];
  editFruit = false;
  fruitForm: FormGroup;
  constructor(private fb: FormBuilder, private fruitService: ApiService) {}
  ngOnInit(): void {
    this.getAll();
    this.fruitForm = this.createForm();
  }
  getAll() {
    this.fruitService
      .getAll()
      .toPromise()
      .then((result) => {
        this.result = result;
        console.log(result);
      });
  }
  createForm() {
    return this.fb.group({
      id: [''],
      name: [''],
      description: [''],
    });
  }
  createPost() {
    const fruit = new Fruit();
    fruit.name = this.fruitForm.controls.name.value;
    fruit.description = this.fruitForm.controls.description.value;
    this.fruitService
      .post(fruit)
      .toPromise()
      .then((res) => {
        this.result.push(res);
        console.log(res);
      });
  }
  delete(fruit) {
    try {
      this.fruitService
        .delete(fruit.id)
        .toPromise()
        .then((res) => {
          console.log('Delete successful');
        });
    } catch (error) {
      console.log('Something went wrong!');
    }
    this.getAll();
  }
  edit(fruit) {
    this.editFruit = true;
    this.fruitForm.setValue(fruit);
  }
  saveUpdatedFruit() {
    this.fruitService
      .update(this.fruitForm.controls.id.value, this.fruitForm.value)
      .toPromise()
      .then((fruit) => {
        this.editFruit = false;
        this.getAll();
        this.fruitForm.reset();
      });
  }
  cancel(){
    this.fruitForm.reset();
    this.editFruit = false;
  }
}
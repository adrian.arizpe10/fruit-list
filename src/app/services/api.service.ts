import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Fruit } from '../models/fruit';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private Http:HttpClient) { }

  getAll(){
    return this.Http.get(`${environment.url}fruits/all`) as Observable<Fruit[]>
  }
  post(fruit: Fruit){
    return this.Http.post(`${environment.url}fruits/create`, fruit) as Observable<Fruit>;
  }
  delete(id: number){
    return this.Http.delete(environment.url + 'fruits/delete/' + id, {responseType:"text"}) as Observable<any>;
  }
  update(id:number, fruit: Fruit){
    return this.Http.put(environment.url + 'fruits/update/' + id,fruit) as Observable<any>;
  }
}

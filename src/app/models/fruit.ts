export class Fruit {
    id: number;
    name: String;
    description: String;
}